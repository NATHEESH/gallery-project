import React from 'react';
const constructUrl = (image) => {
    let farm = image.farm;
    let server = image.server;
    let id = image.id;
    let secret = image.secret;
    let title = image.title;
    let url = `https://farm${farm}.staticflickr.com/${server}/${id}_${secret}_m.jpg`;
    return {url:url,title:title};
}
const Gallery = ({ data }) => {
    return (
        <>
            <ul>
                {
                    data.map((image, index) => {
                        let {url,title} = constructUrl(image);
                        return <li key={index}><img src={url} alt={title}></img></li>;
                    }
                    )
                }
            </ul>
        </>
    )
}
export default Gallery;