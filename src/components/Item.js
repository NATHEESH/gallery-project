import React, {  useEffect } from 'react';
import Gallery from './Gallery';
import Loading from './Loading';
import { connect } from 'react-redux'
import { fetchImages } from '../actions/imagesAction';

const Item = ({fetchImagesHandler,searchTerm,loading,images }) => {
    useEffect(() => {
        fetchImagesHandler(searchTerm);
    }, [searchTerm])
    return (
        <div className="photo-container">
            <h2>{searchTerm} pictures</h2>
            {loading ? <Loading /> : <Gallery data={images} />}
        </div>
    )
}

// const mapStateToProps = state => ({
//     images: state.images.images,
//     loading: state.images.loading
//   })
  
// const mapDispatchToProps = (dispatch)=>{
//    return  {fetchImagesHandler:(searchTerm)=>dispatch(fetchImages(searchTerm))};

// }  
// export default connect(mapStateToProps,mapDispatchToProps)(Item)
export default Item;