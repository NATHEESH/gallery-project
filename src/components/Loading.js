import React from 'react';

const Loading = () => {
    return (
        <div>
            <p>Loading.... Please wait...</p>
        </div>
    )
}
export default Loading;