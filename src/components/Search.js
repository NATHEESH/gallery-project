import React from 'react';
import Item from './Item';
const Search = ({ searchTerm }) => {

    return (
        <>
            <Item searchTerm={searchTerm} />
        </>
    )
}
export default Search;