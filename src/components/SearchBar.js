import React, { useState } from 'react';
const SearchBar = ({handleSubmit,history}) => {
    const [searchItem, setSearchItem] = useState("");
    return (
        <div className="search-form">
            <input type="text" placeholder="Search..." 
            onChange={(event) => {setSearchItem(event.target.value);
            }} value={searchItem}>

            </input>
            <button className="search-button active" style={{color:"white"}} onClick={(e)=>{handleSubmit(e,history,searchItem);
            setSearchItem("");}}>Search
            </button>
        </div>
    )
}
export default SearchBar;