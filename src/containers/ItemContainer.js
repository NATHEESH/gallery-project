
const mapStateToProps = state => ({
    images: state.images.images,
    loading: state.images.loading
  })


  
const mapDispatchToProps = (dispatch)=>{
   return  {fetchImagesHandler:(searchTerm)=>dispatch(fetchImages(searchTerm))};

}  
export default connect(mapStateToProps,mapDispatchToProps)(Item)
